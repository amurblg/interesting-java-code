import java.util.*;

public class HeapImplementation {
    public static void main(String[] args) {
        List<Long> timings = new ArrayList<>(5);
        for (int j = 0; j < 5; j++) {
            long start = System.currentTimeMillis();
            PriorityQueue<Integer> priorityQueue = new PriorityQueue<>();
            Random random = new Random();
            for (int i = 0; i < 1e6; i++) {
                priorityQueue.add(random.nextInt((int) (1e9 + 1)));
            }
            while (priorityQueue.size() > 0) {
                priorityQueue.poll();
            }
            timings.add(System.currentTimeMillis() - start);
            System.out.println("PriorityQueue attempt " + (j + 1) + ": " + timings.get(j) + " ms");
        }
        System.out.println("Average: " + timings.stream().mapToLong(Long::longValue).average().getAsDouble());

        timings.clear();
        for (int j = 0; j < 5; j++) {
            long start = System.currentTimeMillis();
            PriorityQueue<Integer> priorityQueue = new PriorityQueue<>(Comparator.reverseOrder());
            Random random = new Random();
            for (int i = 0; i < 1e6; i++) {
                priorityQueue.add(random.nextInt((int) (1e9 + 1)));
            }
            while (priorityQueue.size() > 0) {
                priorityQueue.poll();
            }
            timings.add(System.currentTimeMillis() - start);
            System.out.println("PriorityQueue (reverse order) attempt " + (j + 1) + ": " + timings.get(j) + " ms");
        }
        System.out.println("Average: " + timings.stream().mapToLong(Long::longValue).average().getAsDouble());

        timings.clear();
        for (int j = 0; j < 5; j++) {
            long start = System.currentTimeMillis();
            MinMaxHeap minMaxHeap = new MinMaxHeap(true);
            Random random = new Random();
            for (int i = 0; i < 1e6; i++) {
                minMaxHeap.add(random.nextInt((int) (1e9 + 1)));
            }
            while (minMaxHeap.size() > 0) {
                minMaxHeap.poll();
            }
            timings.add(System.currentTimeMillis() - start);
            System.out.println("My MinHeap attempt " + (j + 1) + ": " + timings.get(j) + " ms");
        }
        System.out.println("Average: " + timings.stream().mapToLong(Long::longValue).average().getAsDouble());

        timings.clear();
        for (int j = 0; j < 5; j++) {
            long start = System.currentTimeMillis();
            MinMaxHeap minMaxHeap = new MinMaxHeap(false);
            Random random = new Random();
            for (int i = 0; i < 1e6; i++) {
                minMaxHeap.add(random.nextInt((int) (1e9 + 1)));
            }
            while (minMaxHeap.size() > 0) {
                minMaxHeap.poll();
            }
            timings.add(System.currentTimeMillis() - start);
            System.out.println("My MaxHeap attempt " + (j + 1) + ": " + timings.get(j) + " ms");
        }
        System.out.println("Average: " + timings.stream().mapToLong(Long::longValue).average().getAsDouble());

    }
}

class MinMaxHeap<T extends Comparable> {
    private List<T> storage = new ArrayList<T>();
    private boolean minOrMax = true;

    public MinMaxHeap(boolean isMin) {
        minOrMax = isMin;
    }

    @Override
    public String toString() {
        return storage.toString();
    }

    public void print() {
        System.out.println(storage);
    }

    public T peek() {
        return storage.get(0);
    }

    public T poll() {
        if (storage.size() == 0) return null;

        T result = storage.get(0);
        if (storage.size() > 1) {
            storage.set(0, storage.get(storage.size() - 1));
            storage.remove(storage.size() - 1);
            if (storage.size() > 1) {
                int currentIdx = 1;
                int child1Idx = 2;
                int child2Idx = 3;
                while (compare(storage.get(currentIdx - 1), storage.get(idxOfMinMaxValue(child1Idx - 1, child2Idx - 1)))) {
                    int tmp = idxOfMinMaxValue(child1Idx - 1, child2Idx - 1) + 1;
                    swap(currentIdx - 1, tmp - 1);
                    currentIdx = tmp;
                    child1Idx = tmp * 2;
                    child2Idx = 1 + tmp * 2;
                    if (child1Idx - 1 >= storage.size()) break;
                    if (child2Idx - 1 >= storage.size()) {
                        if (compare(storage.get(currentIdx - 1), storage.get(child1Idx - 1)))
                            swap(currentIdx - 1, child1Idx - 1);
                        break;
                    }
                }
            }
        } else {
            storage.remove(0);
        }

        return result;
    }

    private int idxOfMinMaxValue(int idx1, int idx2) {
        if (idx2 >= storage.size()) return idx1;

        if (minOrMax) {
            if (storage.get(idx1).compareTo(storage.get(idx2)) <= 0) return idx1;
            else return idx2;
        } else {
            if (storage.get(idx1).compareTo(storage.get(idx2)) > 0) return idx1;
            else return idx2;
        }
    }

    public void add(T element) {
        storage.add(element);
        int newIndex = storage.size() - 1;

        while (newIndex > 0) {
            int parent = (newIndex - 1) / 2;
            if (compare(storage.get(parent), element)) {
                swap(newIndex, parent);
                newIndex = parent;
            } else break;
        }
    }

    public void add(T... elements) {
        for (int i = 0; i < elements.length; i++) {
            add(elements[i]);
        }
    }

    public int size() {
        return storage.size();
    }

    private boolean compare(T element1, T element2) {
        if (minOrMax) {
            if (element1.compareTo(element2) > 0) return true;
        } else {
            if (element2.compareTo(element1) > 0) return true;
        }
        return false;
    }

    private void swap(int idx1, int idx2) {
        T tmp = storage.get(idx1);
        storage.set(idx1, storage.get(idx2));
        storage.set(idx2, tmp);
    }
}