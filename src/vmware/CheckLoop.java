/*
* https://www.glassdoor.com/Interview/The-first-HackerRank-challenge-I-had-was-an-abstract-design-problem-where-I-was-told-within-90-minutes-to-write-a-functio-QTN_1456765.htm

* Just preparing myself for VMWare interview, search every possible bit of information on the Internet.
* But there's not much results - this is only the second task related to VMWare hiring process I've managed to find.

* Text from the link above:
* The first HackerRank challenge I had was an abstract design  problem where I was told, within 90 minutes, to write a function
* that detects whether a robot loops within a trajectory passed as an arbitrary commands string that controls the movement of the robot.
* (e.g. GRGRGL where L stands for turn left, R for turn right and G for go one step).
* */

package vmware;

import java.util.Scanner;

public class CheckLoop {
    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
        String robotPath = s.nextLine();
    }

    public static boolean pathIsLooped(String robotPath) {
        //Algorithm:
        //Analyze string from right to left; check if there repetitive substrings with length from 1 till n/2

        return false;
    }
}
