/*
* Interesting task, found in Google - VMWare pre-interview Evaluation challenge. Found it here: http://ajgupta.github.io/interview/2014/11/01/VMWare-challenge-on-HackerRank/

You are given an integer array with N elements: d[0], d[1], ... d[N - 1].
You can perform AT MOST one move on the array: choose any two integers [L, R], and flip all the elements between (and including) the L-th and R-th bits. L and R represent the left-most and right-most index of the bits marking the boundaries of the segment which you have decided to flip.

What is the maximum number of '1'-bits (indicated by S) which you can obtain in the final bit-string?

'Flipping' a bit means, that a 0 is transformed to a 1 and a 1 is transformed to a 0 (0->1,1->0).
Input Format
An integer N
Next line contains the N bits, separated by spaces: d[0] d[1] ... d[N - 1]

Output:
S

Constraints:
1 <= N <= 100000
d[i] can only be 0 or 1
0 <= L <= R < n

Sample Input:
8
1 0 0 1 0 0 1 0

Sample Output:
6

Explanation:

We can get a maximum of 6 ones in the given binary array by performing either of the following operations:
Flip [1, 5] ==> 1 1 1 0 1 1 1 0

Didn't watch code provided by link above - tried to solve it myself.
*/

package vmware;

import java.util.Scanner;

public class FlippingBits {
    public static void main(String[] args) {
        //Scanner s = new Scanner(System.in);
        Scanner s = new Scanner("8\n" +
                "1 0 0 1 0 0 1 0");
        int n = s.nextInt();
        s.nextLine();
        String bits = s.nextLine().replaceAll(" +", "");

        //So, our goal, as I understand it - to find substring with maximum difference between 0s and 1s - i.e. [amount of zeroes minus amount of ones] = maximum possible for this string.
        //NB: plus, we need substring meeting that condition with minimum possible length!
        int len = bits.length();
        int maxStart = 0;
        int maxEnd = 0;
        int maxDifference = Integer.MIN_VALUE;

        for (int i = 0; i < len; i++) {
            for (int j = len; j > i; j--) {
                int t = get0and1difference(bits.substring(i, j));
                if (t > maxDifference) {
                    maxStart = i;
                    maxEnd = j;
                    maxDifference = t;
                } else if (t == maxDifference && j - i < maxEnd - maxStart) {
                    maxStart = i;
                    maxEnd = j;
                    maxDifference = t;
                }
            }
        }

        System.out.println(
                bits.chars().filter(e -> e == '1').count()//Total amount of ones in whole string
                        + bits.substring(maxStart, maxEnd).chars().filter(e -> e == '0').count()//plus amount of zeroes in discovered substring - they'll turn into ones after flipping
                        - bits.substring(maxStart, maxEnd).chars().filter(e -> e == '1').count()//minus amount of ones in discovered substring - they'll turn into zeroes after flipping
        );
    }

    private static int get0and1difference(String substring) {
        return (int) (substring.chars().filter(e -> e == '0').count() - substring.chars().filter(e -> e == '1').count());
    }
}
