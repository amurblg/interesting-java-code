import java.util.Arrays;
import java.util.Scanner;

/**
 * https://stepik.org/lesson/Быстрая-сортировка-13249/step/6?course=Алгоритмы-теория-и-практика-Методы&unit=3434
 */
public class PointsAndSegmentsIssue {
    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);
//        Scanner scanner = new Scanner(new String("2 3\n" +
//                "0 5\n" +
//                "7 10\n" +
//                "1 6 11"));//Simple test case
        Scanner scanner = new Scanner(new String("6 5\n" +
                "1 6\n" +
                "2 5\n" +
                "3 7\n" +
                "3 6\n" +
                "4 10\n" +
                "12 13\n" +
                "3 7 9 11 10"));//Simple test case
        int n = scanner.nextInt();
        int m = scanner.nextInt();
        Segment[] segments = new Segment[n];
        Segment_byEnd[] segments_byEnd = new Segment_byEnd[n];

        for (int i = 0; i < n; i++) {
            segments[i] = new Segment(scanner.nextInt(), scanner.nextInt());
            segments_byEnd[i] = new Segment_byEnd(segments[i]);
        }

        QuickSort<Segment> quickSort = new QuickSort<>();
        QuickSort<Segment_byEnd> quickSort_byEnd = new QuickSort<>();
        quickSort.qsort(segments, 0, n - 1);
        quickSort_byEnd.qsort(segments_byEnd, 0, n - 1);

        Arrays.stream(segments).forEach(e -> System.out.print(e + " "));
        System.out.println();
        Arrays.stream(segments_byEnd).forEach(e -> System.out.print(e + " "));
        System.out.println();

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < m; i++) {
            int point = scanner.nextInt();
            int a = 0;
            int b = 0;
            for (int j = 0; j < n; j++) {
                if (segments[j].getStart() <= point) a++;
                if (segments[j].getStart() > point) break;
            }
            for (int j = 0; j < n; j++) {
                if (segments_byEnd[j].getEnd() < point) b++;
                if (segments_byEnd[j].getEnd() >= point) break;
            }
            stringBuilder.append(a-b).append(" ");
        }
        System.out.println(stringBuilder.toString());//Should print "1 0 0" for test case above
    }
}

class Segment implements Comparable {
    private int start;
    private int end;

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public Segment(int start, int end) {

        this.start = start;
        this.end = end;
    }

    @Override
    public String toString() {
        return "[" + start + "; " + end + "]";
    }

    @Override
    public int compareTo(Object o) {
        return start - ((Segment) o).start;
    }
}

class Segment_byEnd implements Comparable {
    private int start;
    private int end;

    public int getStart() {
        return start;
    }

    public int getEnd() {
        return end;
    }

    public Segment_byEnd(Segment segment) {
        start = segment.getStart();
        end = segment.getEnd();
    }

    @Override
    public String toString() {
        return "[" + start + "; " + end + "]";
    }

    public Segment_byEnd(int start, int end) {
        this.start = start;
        this.end = end;
    }

    @Override
    public int compareTo(Object o) {
        return end - ((Segment_byEnd) o).end;
    }
}