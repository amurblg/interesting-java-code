import java.util.Arrays;

/**
 * https://stepik.org/lesson/%D0%91%D1%8B%D1%81%D1%82%D1%80%D0%B0%D1%8F-%D1%81%D0%BE%D1%80%D1%82%D0%B8%D1%80%D0%BE%D0%B2%D0%BA%D0%B0-13249/step/1?course=%D0%90%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC%D1%8B-%D1%82%D0%B5%D0%BE%D1%80%D0%B8%D1%8F-%D0%B8-%D0%BF%D1%80%D0%B0%D0%BA%D1%82%D0%B8%D0%BA%D0%B0-%D0%9C%D0%B5%D1%82%D0%BE%D0%B4%D1%8B&unit=3434
 */
public class QuickSort<T extends Comparable> {
    public static void main(String[] args) {
        QuickSort<Integer> test = new QuickSort<>();
        Integer[] array = new Integer[]{4, 3, 2, 5, 1};
        Arrays.stream(array).forEach(e -> System.out.print(e + " "));//4 3 2 5 1
        test.qsort(array, 0, array.length - 1);
        System.out.println();
        Arrays.stream(array).forEach(e -> System.out.print(e + " "));//1 2 3 4 5
    }

    public void qsort(T[] array, int l, int r) {
        if (l >= r) return;
        int m = partition(array, l, r);
        qsort(array, l, m - 1);
        qsort(array, m + 1, r);
    }

    private int partition(T[] array, int l, int r) {
        T x = array[l];
        int j = l;
        for (int i = l + 1; i <= r; i++) {
            if (array[i].compareTo(x) < 0) {
                j++;
                T tmp = array[i];
                array[i] = array[j];
                array[j] = tmp;
            }
        }
        T tmp = array[l];
        array[l] = array[j];
        array[j] = tmp;
        return j;
    }
}