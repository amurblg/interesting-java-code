/**
 * https://stepik.org/lesson/Сортировка-слиянием-13248/step/5?course=Алгоритмы-теория-и-практика-Методы&unit=3433
 */

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Scanner;

public class ArrayInversionCalc {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int n = scanner.nextInt();
        Integer[] array = new Integer[n];
        for (int i = 0; i < n; i++) {
            array[i] = scanner.nextInt();
        }

        ArrayInversionCalc calc = new ArrayInversionCalc();
        System.out.println(calc.inversionCount(array));
    }



    public Integer[] mergeSort(Integer[] array) {
        LinkedList<Integer[]> list = new LinkedList<>();
        Arrays.stream(array).forEach(e -> list.add(new Integer[]{e}));
        while (list.size() > 1) {
            Integer[] a = list.poll();
            Integer[] b = list.poll();
            Integer[] c = new Integer[a.length + b.length];
            int nextA = 0;
            int nextB = 0;
            int nextC = 0;
            while (nextC < c.length) {
                if (a[nextA] <= b[nextB]) {
                    c[nextC++] = a[nextA++];
                    if (nextA >= a.length) for (int i = nextB; i < b.length; i++) c[nextC++] = b[i];
                } else {
                    c[nextC++] = b[nextB++];
                    if (nextB >= b.length) for (int i = nextA; i < a.length; i++) c[nextC++] = a[i];
                }
            }
            list.add(c);
        }
        return list.get(0);
    }

    public long inversionCount(Integer[] array) {
        int pow2 = 1;
        while (pow2 < array.length) {
            pow2 *= 2;
        }
        int bigInt = (int) (1e9 + 1);

        LinkedList<Integer[]> list = new LinkedList<>();
        Arrays.stream(array).forEach(e -> list.add(new Integer[]{e}));
        if (pow2 > 1) {
            for (int i = 0; i < pow2 - array.length; i++) {
                list.add(new Integer[]{bigInt});
            }
        }

        long permCounter = 0;
        while (list.size() > 1) {
            Integer[] a = list.poll();
            Integer[] b = list.poll();
            Integer[] c = new Integer[a.length + b.length];
            int nextA = 0;
            int nextB = 0;
            int nextC = 0;
            while (nextC < c.length) {
                if (a[nextA] <= b[nextB]) {
                    c[nextC++] = a[nextA++];
                    if (nextA >= a.length) for (int i = nextB; i < b.length; i++) c[nextC++] = b[i];
                } else {
                    c[nextC++] = b[nextB++];
                    permCounter += a.length - nextA;
                    if (nextB >= b.length) for (int i = nextA; i < a.length; i++) c[nextC++] = a[i];
                }
            }
            list.add(c);
        }
        return permCounter;
    }
}