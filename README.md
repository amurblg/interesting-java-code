# README #

###I've decided to make a distinct storage for different code which I assume needs to be saved: interesting algorithms implementations, code hacks, snippets and so on. All code here so far is in Java (Java8 or older). ###
## Contents ##
## Binomial Heap - my own implementation.
Very [first commit](https://bitbucket.org/amurblg/interesting-java-code/commits/5383cc3431201fdcb4c281673bb1b04ea9e70e94) here - [interesting task](https://stepik.org/lesson/%D0%9E%D1%87%D0%B5%D1%80%D0%B5%D0%B4%D0%B8-%D1%81-%D0%BF%D1%80%D0%B8%D0%BE%D1%80%D0%B8%D1%82%D0%B5%D1%82%D0%B0%D0%BC%D0%B8-13240/step/8?unit=3426) from online video-course "Algorithms: theory and practice" at stepic.org (course is in Russian language).
It propose to implement your own version of binomial heap (max-heap, to be precise).

[Theory (pdf, 54 pages) from princeton.edu](https://www.cs.princeton.edu/courses/archive/spring13/cos423/lectures/BinomialHeaps.pdf)

[Binary Heap Visualiser](http://btv.melezinek.cz/binary-heap.html) - online tool for testing purposes. Helped a lot...

## Arrays Merge sorting, arrays inversion counter
[The next commit](https://bitbucket.org/amurblg/interesting-java-code/commits/dfa36f3acd81bdecaa2371b337c4757802a445c8) - another interesting issue, implementation of arrays merge sorting as well as arrays "Inversion counter" (see [wiki](https://en.wikipedia.org/wiki/Inversion_(discrete_mathematics))).
 Inspired by [this lesson](https://stepik.org/lesson/Сортировка-слиянием-13248/step/5?course=Алгоритмы-теория-и-практика-Методы&unit=3433) (Russian languge) in online course "Algorithms: theory and practice" at stepic.org.
 
 